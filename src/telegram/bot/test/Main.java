package telegram.bot.test;

import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;

import telegram.bot.test.handler.CommandHandler;

public class Main {

	public static void main(String[] args) {
		
		ApiContextInitializer.init();
		TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
		
		try {
			telegramBotsApi.registerBot(new CommandHandler());
		} catch (TelegramApiRequestException e) {
			e.printStackTrace();
		}
	}

}
