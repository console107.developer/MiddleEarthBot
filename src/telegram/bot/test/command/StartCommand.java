package telegram.bot.test.command;

import java.util.ArrayList;
import java.util.List;

import org.telegram.telegrambots.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.api.objects.CallbackQuery;
import org.telegram.telegrambots.api.objects.CallbackSender;
import org.telegram.telegrambots.api.objects.Chat;
import org.telegram.telegrambots.api.objects.User;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.bots.AbsSender;
import org.telegram.telegrambots.bots.commands.BotCommand;
import org.telegram.telegrambots.exceptions.TelegramApiException;

public class StartCommand extends BotCommand{
	
	// codes to filter callbacks
	private static final String COMPANION_OK = "A";
	private static final String DIRECTION_OK = "B";
	private static final String TIME_OK = "C";
	
	public StartCommand() {
		super("start", "the start command");
	}

	@Override
	public void execute(AbsSender absSender, User user, Chat chat, String[] arguments) {
		
		SendMessage sendMessage = new SendMessage();
		InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
		List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();
		String[] companion = new String[]{"Gandalf","Gimli","Aragorn","Legolas"};
		
		for(int i=0; i<companion.length; i++){
			
			CallbackSender callbackSender = new CallbackSender();		// CallbackSender help you to build a custom callback
			callbackSender.setReceiver(this.getCommandIdentifier());	// callback will be sent to this class
			callbackSender.setCode(COMPANION_OK);								// code to filter callbacks
			callbackSender.setData(companion[i] + " ask:");							// data for this callback
			
			InlineKeyboardButton busLineButton = new InlineKeyboardButton();
			busLineButton.setText(companion[i]);
			busLineButton.setCallbackData(callbackSender);				// CallbackSender is structured as json tree so we can pass it as a string
			
			List<InlineKeyboardButton> row = new ArrayList<>();
			row.add(busLineButton);
			
			keyboard.add(row);
		}
		
		inlineKeyboardMarkup.setKeyboard(keyboard);
		
		sendMessage.setText("<b>Midde-Earth travel</b>\n<i>Choose a travel companion</i>");
		sendMessage.setChatId(chat.getId());
		sendMessage.enableHtml(true);
		sendMessage.setReplyMarkup(inlineKeyboardMarkup);
		
		try {
			absSender.sendMessage(sendMessage);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This new method allow user to handle callbacks easy
	 */
	@Override
	public void handleCallback(AbsSender absSender, CallbackQuery callbackQuery) {
		
		CallbackSender callbackSender = callbackQuery.getCallbackSender();	// get the CallbackSender from the CallbackQuery
		String code = callbackSender.getCode();								// get callback code							
		String data = callbackSender.getData();								// get callback data
		
		AnswerCallbackQuery answerCallbackQuery = new AnswerCallbackQuery();			
		EditMessageText editMessageText = new EditMessageText();
		InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
		List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();
		
		switch(code){														// choice filtered by code
		
		case COMPANION_OK:
			
			String[] directions = new String[]{"Gondor","Mordor"};
			
			for(int i=0; i<directions.length; i++){
				
				callbackSender = new CallbackSender();						// resetting callbackSender
				callbackSender.setReceiver(this.getCommandIdentifier());	// callback will be sent to this class again
				callbackSender.setCode(DIRECTION_OK);								// set a new code
				callbackSender.setData("Road to " + directions[i]);			// data for this callback
				
				InlineKeyboardButton busLineButton = new InlineKeyboardButton();
				busLineButton.setText(directions[i]);
				busLineButton.setCallbackData(callbackSender);				// CallbackSender is structured as json tree so we can pass it as a string
				
				List<InlineKeyboardButton> row = new ArrayList<>();
				row.add(busLineButton);
				
				keyboard.add(row);
			}
			
			inlineKeyboardMarkup.setKeyboard(keyboard);
			
			editMessageText.setMessageId(callbackQuery.getMessage().getMessageId());
			editMessageText.setText("<b>" + data + "</b>\n<i>Where do you want to go?</i>");
			editMessageText.setChatId(callbackQuery.getMessage().getChatId());
			editMessageText.enableHtml(true);
			editMessageText.setReplyMarkup(inlineKeyboardMarkup);
	
			answerCallbackQuery.setCallbackQueryId(callbackQuery.getId());
			answerCallbackQuery.setText(data);

			try {
				absSender.answerCallbackQuery(answerCallbackQuery);
				absSender.editMessageText(editMessageText);
			} catch (TelegramApiException e) {
				e.printStackTrace();
			}
			break;
			
		case DIRECTION_OK:
			
			String[] time = new String[]{"05:10","06.10","15.30"};
			
			for(int i=0; i<time.length; i++){
				
				callbackSender = new CallbackSender();						// resetting callbackSender
				callbackSender.setReceiver(this.getCommandIdentifier());	// callback will be sent to this class again
				callbackSender.setCode(TIME_OK);								// set a new code
				callbackSender.setData(time[i]);			// data for this callback
				
				InlineKeyboardButton busLineButton = new InlineKeyboardButton();
				busLineButton.setText(time[i]);
				busLineButton.setCallbackData(callbackSender);				// CallbackSender is structured as json tree so we can pass it as a string
				
				List<InlineKeyboardButton> row = new ArrayList<>();
				row.add(busLineButton);
				
				keyboard.add(row);
			}
			
			inlineKeyboardMarkup.setKeyboard(keyboard);
			
			editMessageText.setMessageId(callbackQuery.getMessage().getMessageId());
			editMessageText.setText("<b>" + data + "</b>\n<i>What time do you want to leave?</i>");
			editMessageText.setChatId(callbackQuery.getMessage().getChatId());
			editMessageText.enableHtml(true);
			editMessageText.setReplyMarkup(inlineKeyboardMarkup);
	
			answerCallbackQuery.setCallbackQueryId(callbackQuery.getId());
			answerCallbackQuery.setText(data);

			try {
				absSender.answerCallbackQuery(answerCallbackQuery);
				absSender.editMessageText(editMessageText);
			} catch (TelegramApiException e) {
				e.printStackTrace();
			}
			break;
			
		case TIME_OK:
			
			editMessageText.setMessageId(callbackQuery.getMessage().getMessageId());
			editMessageText.setText("<b>Thank you little hobbit</b>\n<i>The trip start at " + data +"</i>");
			editMessageText.setChatId(callbackQuery.getMessage().getChatId());
			editMessageText.enableHtml(true);
			editMessageText.setReplyMarkup(inlineKeyboardMarkup);
			
			answerCallbackQuery.setCallbackQueryId(callbackQuery.getId());
			answerCallbackQuery.setText(data);
			
			try {
				absSender.answerCallbackQuery(answerCallbackQuery);
				absSender.editMessageText(editMessageText);
			} catch (TelegramApiException e) {
				e.printStackTrace();
			}
			break;
		}
	}
}
