package telegram.bot.test;

public class BotConfig {
	
	// Telegram bot token
	public static final String BOT_TOKEN = "YOUR_BOT_TOKEN";
	// Telegram bot username
	public static final String BOT_USERNAME = "YOUR_BOT_USERNAME";

}
