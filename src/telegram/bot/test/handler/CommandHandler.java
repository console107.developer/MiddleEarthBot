package telegram.bot.test.handler;

import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingCommandBot;

import telegram.bot.test.BotConfig;
import telegram.bot.test.command.StartCommand;

public class CommandHandler extends TelegramLongPollingCommandBot{
	
	public CommandHandler() {
		
		register(new StartCommand());
	}

	@Override
	public void processNonCommandUpdate(Update update) {
	

	}

	@Override
	public String getBotToken() {

		return BotConfig.BOT_TOKEN;
	}
	
	
	@Override
	public String getBotUsername() {
		
		return BotConfig.BOT_USERNAME;
	}

}
